import time

class Jeux:
    def __init__(self,filename=None):
        self.jeux_init()


    def jeux_init(self):    
        self.etat_c = [(5, 0)]

        
        self.joueur_tour = 'P1'

    def draw_board(self):
        for p in self.etat_c:
            print("(%d, %d)" % p, end="--")
        print()

    
    def is_valid(self, n, a, b):
        return a + b == n

    
    def is_end(self):
        a, b = self.etat_c[-1]
        if a != b: return None
        a = ['P2', 'P1'][len(self.etat_c) % 2]
        return a

   
    def max(self, alpha, beta):

        maxv = -2

        a = None
        b = None

        result = self.is_end()


        if result == 'P1':
            return -1, self.etat_c[-1][0], self.etat_c[-1][1]
        elif result == 'P2':
            return 1, self.etat_c[-1][0], self.etat_c[-1][1]


        n = self.etat_c[-1][0]


        for i in range(1, n//2+1):
.
            self.etat_c.append((max(n - i, i), min(n - i, i)))
            m, min_a, min_b = self.min(alpha, beta)

            if m > maxv:
                a, b = (max(n - i, i), min(n - i, i))
                maxv = m


            self.etat_c.pop()
        return maxv, a, b


    def min(self, alpha, beta):


        minv = 2

        a = None
        b = None

        result = self.is_end()


        if result == 'P1':
            return -1,  self.etat_c[-1][0], self.etat_c[-1][1]
        elif result == 'P2':
            return 1, self.etat_c[-1][0], self.etat_c[-1][1]


        n = self.etat_c[-1][0]

        for i in range(1, n//2+1):

            self.etat_c.append((max(n - i, i), min(n - i, i)))
            m, min_a, min_b = self.max(alpha, beta)

            if m < minv:
                minv = m
                a, b = (max(n - i, i), min(n - i, i))

            self.etat_c.pop()



        return minv, a, b

    def jouer(self):
        while True:
            self.draw_board()
            self.result = self.is_end()

            if self.result is not None:
                if self.result == 'P1':
                    print('The winner is P1!')
                elif self.result == 'P2':
                    print('The winner is P2!')

                self.jeux_init()
                return


            if self.joueur_tour == 'P1':

                while True:

                    start = time.time()
                    m, a, b = self.min(-2, 2)
                    end = time.time()
                    print('Evaluation time: {}s'.format(round(end - start, 7)))
                    print('Recommended move: X = {}, Y = {}'.format(a, b))

                    a = int(input('a: '))
                    b = int(input('b: '))

                    s = (a, b)


                    n = self.etat_c[-1][0]

                    if self.is_valid(n, a, b):
                        self.etat_c.append((max(s), min(s)))
                        self.joueur_tour = 'P2'
                        break
                    else:
                        print('The move is not valid! Try again.')

            else:
                (m, a, b) = self.max(-2, 2)
                s = (a, b)
                self.etat_c.append((max(s), min(s)))
                self.joueur_tour = 'P1'